package main;

// Here is a useless change.

/**Class which holds the name and type of a card
 * 
 * @author James
 *
 */
public class Card {
	
	/**Name of the card */
	private String _cardName;
	
	/**Type of the card (weapon, room, character) */
	private String _cardType;

	/**Creates a card object of type d named s
	 * 
	 *  @param s name of card
	 *  @param d type of card*/
	public Card(String s, String d){
		_cardName = s;
		_cardType = d;
	}
	
	/**Accessor method for the name of the card
	 * 
	 *  @return Name of the card*/
	public String getName(){
		return _cardName;
	}
	
	/**Accessor method for the type of the card
	 * 
	 *  @return Type of the card*/
	public String getType(){
		return _cardType;
	}
	
	//Setter methods are not required as there is no reason the change
	//	the values in a card after it is created.
}
