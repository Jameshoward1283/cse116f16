package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class GUI {
	private Model _m;

	public GUI(Model m) {
		_m = m;
		_m.setGUI(this);
		JFrame window = new JFrame();
		window.setVisible(true);

		// 3 rows 1 column
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(3, 1));

		// Player panel holds the current player whose turn it is
		// and the cards they hold
		JPanel playerPanel = new JPanel();
		// Board panel to hold the board and the player pieces on the board
		JPanel boardPanel = new JPanel();
		boardPanel.setLayout(new GridLayout(25, 24));
		// movement panel to hold the movement and suggestion items
		JPanel movementPanel = new JPanel();

		// player panel has the character whose turn it is and the cards after
		// the character
		playerPanel.setLayout(new FlowLayout());
		playerPanel.add(new JButton("Character: "));

		m.fillBoard();
		// iterate through the grid created from fillBoard and make the rooms
		// black and make the non rooms white
		for (int row = 0; row < 25; row++) {
			for (int col = 0; col < 24; col++) {
				if (m.getBoard()[row][col].equals(m.getRoom())) {
					// spot is a room so color it black for now
					JButton button = new JButton();
					button.setBackground(Color.BLACK);
					boardPanel.add(button);
				} else {
					// spot is not a room so color it white for now
					JButton button = new JButton();
					button.setBackground(Color.WHITE);
					boardPanel.add(button);
				}
			}
		}
	}

}

package main;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

public class GUI {
	Model _model;
	JPanel _mainPanel;
	JPanel _playerPanel;
	JPanel _movementPanel;
	JPanel _buttonGrid;
	JFrame _window;
	
	static final int ROWS = 25;
	static final int COLS = 24;
	static final int BUTTON_SIZE = 40;
	
	public GUI(Model m)
	{
		_model = m;
		initialize();
		updateBoard();
		updatePlayerPanel();
		updateMovementPanel();
	}
	
	/**
	 * create things only needed to be created once
	 */
	public void initialize()
	{
		_window = new JFrame();
		
		//3 rows 1 column
		_mainPanel = new JPanel();
		_mainPanel.setLayout(new GridLayout(3,1));
		
		//top row is the player panel that holds the current player and the cards
		_playerPanel = new JPanel();
		_playerPanel.setLayout(new FlowLayout());
		_mainPanel.add(_playerPanel);
		
		//middle row contains the board and the pieces for the board
		_buttonGrid = new JPanel();
		_buttonGrid.setLayout(new GridLayout(ROWS, COLS));
		_mainPanel.add(_buttonGrid);
		
		//bottom row contains buttons for movement for the characters and the suggestion stuff too
		_movementPanel = new JPanel();
		_movementPanel.setLayout(new FlowLayout());
		_mainPanel.add(_movementPanel);
		
		_window.setContentPane(_mainPanel);
		_window.setVisible(true);
	}
	
	/**
	 * update creates the board with rooms being black and the hallways being white for the time being
	 */
	public void updateBoard()
	{
		_buttonGrid.removeAll();
		_model.fillBoard();
		
		//iterate through the grid created from fillBoard and make the rooms black and make the non rooms white
		for (int row = 0; row < 25; row++)
		{
			for (int col = 0; col < 24; col++)
			{
				if (_model.getBoard()[row][col].equals(_model.getIfRoom()))
				{
					//spot is a room so color it black for now
					JButton button = new JButton();
					button.setBackground(Color.BLACK);
					_buttonGrid.add(button);
					
				}
				else
				{
					//spot is not a room so color it white for now
					JButton button = new JButton();
					button.setBackground(Color.GRAY);
					_buttonGrid.add(button);
				}
			}
		}
		
		_window.pack();
		
		
	}
	
	public void updatePlayerPanel()
	{
		String p = getCurrentPlayer();
		_playerPanel.add(new JLabel(p));
		//need to find a way to access all the cards held by the current player
	}
	public String getCurrentPlayer()
	{
		String p = "";
		if (_model.returnCurrentPlayer() == 1)
		{
			p = "Miss Scarlet";
		}
		else if (_model.returnCurrentPlayer() == 2)
		{
			p = "Prof. Plum";
		}
		else if (_model.returnCurrentPlayer() == 3)
		{
			p = "Mr. Green";
		}
		else if (_model.returnCurrentPlayer() == 4)
		{
			p = "Mrs. White";
		}
		else if (_model.returnCurrentPlayer() == 5)
		{
			p = "Mrs. Peacock";
		}
		else if (_model.returnCurrentPlayer() == 6)
		{
			p = "Col. Mustard";
		}
		return p;
	}
	
	public void updateMovementPanel()
	{
		//4 buttons for movement
		//button for secret passage while in room only!
		
		JButton Up = new JButton("Up");
		JButton Down = new JButton("Down");
		JButton Left = new JButton("Left");
		JButton Right = new JButton("Right");
		_movementPanel.add(Up);
		_movementPanel.add(Down);
		_movementPanel.add(Left);
		_movementPanel.add(Right);
		
		//upon clicking these buttons you will move in that direction
		Up.addActionListener(new );
		Down.addActionListener(new );
		Left.addActionListener(new );
		Right.addActionListener(new );
		
		JButton SercretPassage = new JButton("Secret Passage");
		_movementPanel.add(SercretPassage);
		
		SecretPassage.addActionListener(new );
	}
}

