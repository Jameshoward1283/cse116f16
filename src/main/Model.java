package main;

import java.util.ArrayList;
import java.util.Random;

/**
 * Class which acts as the underlying processes of the game, but nothing visual.
 * 
 * @author Charles Henneberger
 */

public class Model {

	
	private GUI _g;
	/**
	 * Array that stores Room objects and acts as the main playing space for the
	 * game.
	 */
	private Room[][] _board;

	/**
	 * Three room objects that _board will store and will be read as the player
	 * moves to prevent illegal game actions.
	 */
	private Room _corridor = new Room();
	private Room _door = new Room();
	private Room _room = new Room();

	/** The Player instance that is controlled by the user. */
	private Player _play;

	/** Index of the player whose turn it currently is */
	private int _p;

	/** Array of all players, including user */
	private Player[] _players;

	/** Array of players that the computer controls. */
	private Player[] _cpus;

	/**
	 * String storing the name of the room or hallway for the purpose of making
	 * guesses and prevents illegal moves.
	 */
	private String _playerLocation;

	/**
	 * Stores a value that is returned from the user during their turn to be
	 * used in the movePlayer method.
	 */
	private String _direct;

	/** Stores the players selected move if they are moving from a room */
	private int[] _roomMove = new int[2];

	/** Stores the number rolled. */
	private int _roll = 0;

	/** player count so only four players can be in the game */
	private int _pcount = 0;

	/** Creates the main class that the game will use. */
	public Model() {
		
		_board = new Room[25][24];
		_players = new Player[6];
		fillBoard();

	}

	/**
	 * Method which fills the board with Room objects in order to create the
	 * playing area for the game.
	 */
	public void fillBoard() {
		for (int i = 0; i < _board.length; i++) {
			for (int j = 0; j < _board[i].length; j++) {
				_board[i][j] = _corridor;
			}
		}

		// Study
		for (int j = 0; j < 7; j++) {
			for (int i = 0; i < 4; i++) {
				_board[i][j] = _room;
			}
		}
		_board[3][6] = _door;

		// Hall
		for (int j = 9; j < 15; j++) {
			for (int i = 0; i < 7; i++) {
				_board[i][j] = _room;
			}
		}
		_board[4][9] = _door;
		_board[6][11] = _door;
		_board[6][12] = _door;

		// Lounge
		for (int j = 18; j < 24; j++) {
			for (int i = 0; i < 6; i++) {
				_board[i][j] = _room;
			}
		}
		_board[5][18] = _door;

		// Library
		for (int j = 0; j < 7; j++) {
			for (int i = 6; i < 11; i++) {
				_board[i][j] = _room;
			}
		}
		_board[6][6] = _corridor;
		_board[10][6] = _corridor;
		_board[8][6] = _door;
		_board[10][3] = _door;

		// Billiard Room
		for (int j = 0; j < 6; j++) {
			for (int i = 12; i < 17; i++) {
				_board[i][j] = _room;
			}
		}
		_board[12][1] = _door;
		_board[15][5] = _door;

		// Conservatory
		for (int j = 0; j < 6; j++) {
			for (int i = 19; i < 24; i++) {
				_board[i][j] = _room;
			}
		}
		_board[19][4] = _door;

		// Ballroom
		for (int j = 8; j < 16; j++) {
			for (int i = 17; i < 23; i++) {
				_board[i][j] = _room;
			}
		}
		for (int j = 10; j < 14; j++) {
			for (int i = 23; i < 25; i++) {
				_board[i][j] = _room;
			}
		}
		_board[19][8] = _door;
		_board[19][15] = _door;
		_board[17][9] = _door;
		_board[17][14] = _door;

		// Kitchen
		for (int j = 19; j < 24; j++) {
			for (int i = 18; i < 24; i++) {
				_board[i][j] = _room;
			}
		}
		_board[18][19] = _door;

		// Dining Room
		for (int j = 17; j < 24; j++) {
			for (int i = 9; i < 15; i++) {
				_board[i][j] = _room;
			}
		}
		for (int j = 20; j < 24; j++) {
			_board[15][j] = _room;
		}
		_board[12][16] = _door;
		_board[9][17] = _door;

		// Thingy in the middle
		for (int j = 9; j < 14; j++) {
			for (int i = 8; i < 15; i++) {
				_board[i][j] = _room;
			}
		}

		_board[0][8] = _room;
		_board[0][16] = _room;
		_board[6][23] = _room;
		_board[8][23] = _room;
		_board[16][23] = _room;
		_board[23][18] = _room;
		_board[24][18] = _room;
		_board[24][17] = _room;
		_board[0][16] = _room;
		_board[23][6] = _room;
		_board[24][6] = _room;
		_board[24][7] = _room;
		_board[24][8] = _room;
		_board[17][0] = _room;
		_board[11][0] = _room;
		_board[4][0] = _room;
	}

	/**
	 * The order in which actions take place during each turn (roll dice, move,
	 * make guess). Also increments the current player at the end of the turn
	 */
	public void turn() {
		// todo
	}

	/**
	 * Updates the players coordinates and location as they traverse the board
	 * 
	 * @param roll
	 *            is the amount of spaces the player can move
	 * @param p
	 *            is the Player who is moving
	 * 
	 * @return Whether or not the move was legal.
	 */
	public boolean movePlayer(String direct, Player p) {
		int x = p.getPlayerXLocation();
		int y = p.getPlayerYLocation();
		switch (direct) {
		case "up":
			if ((y - 1 < 0) || _board[y - 1][x] == _room) {
				return false;
			}
			y -= 1;
			p.setPlayerYLocation(y);
			if (_board[y][x] == _door) {
				intoRoom(p);
			}
			return true;
		case "left":
			if ((x - 1 < 0) || _board[y][x - 1] == _room) {
				return false;
			}
			x -= 1;
			p.setPlayerXLocation(x);
			if (_board[y][x] == _door) {
				intoRoom(p);
			}
			return true;
		case "down":
			if (y + 1 > 24 || _board[y + 1][x] == _room) {
				return false;
			}
			y += 1;
			p.setPlayerYLocation(y);
			if (_board[y][x] == _door) {
				intoRoom(p);
			}
			return true;
		case "right":
			if (x + 1 > 23 || _board[y][x + 1] == _room) {
				return false;
			}
			x += 1;
			p.setPlayerXLocation(x);
			if (_board[y][x] == _door) {
				intoRoom(p);
			}
			return true;
		default:
			return false;
		}

	}

	/**
	 * Provides an array with the possible move locations when the player is in
	 * a room
	 * 
	 * @param p
	 *            The player who is moving out of a room.
	 * 
	 * @return ArrayList<Integer> of possible moves
	 */
	public ArrayList<Integer> moveFromRoom(Player p) {
		ArrayList<Integer> a = new ArrayList<Integer>();
		if (p.getLocation() == "Hallway") {

		}
		switch (p.getLocation()) {
		case "Study":
			a.add(4);
			a.add(6);
			break;
		case "Hall":
			a.add(4);
			a.add(8);
			a.add(7);
			a.add(11);
			a.add(7);
			a.add(12);
			break;
		case "Lounge":
			a.add(6);
			a.add(18);
			break;
		case "Library":
			a.add(8);
			a.add(7);
			a.add(11);
			a.add(13);
			break;
		case "Billiard Room":
			a.add(11);
			a.add(1);
			a.add(15);
			a.add(6);
			break;
		case "Conservatory":
			a.add(19);
			a.add(5);
			break;
		case "Ballroom":
			a.add(19);
			a.add(7);
			a.add(19);
			a.add(16);
			a.add(16);
			a.add(9);
			a.add(16);
			a.add(14);
			break;
		case "Kitchen":
			a.add(17);
			a.add(19);
			break;
		case "Dining Room":
			a.add(8);
			a.add(17);
			a.add(12);
			a.add(15);
			break;
		}
		return a;
		// this method is for use in the GUI so it can provide coordinates to
		// the ExitRoom event class
	}

	/**
	 * Allows the gui to set _direct for the purpose of moving
	 * 
	 * @param s
	 *            string representing the direction of the move
	 */
	public void setDirect(String s) {
		_direct = s;
	}

	/**
	 * Updates rmName inside the Player object to represent the room the player
	 * has entered. Is used for making guesses, taking secret passages, and
	 * moving.
	 * 
	 * @param p
	 *            Player whose location is changing
	 */
	public void intoRoom(Player p) {
		int x = p.getPlayerXLocation();
		int y = p.getPlayerYLocation();
		if (x == 6 && y == 3) {
			p.setLocation("Study");
		}
		if ((x == 9 && y == 4) || (x == 11 && y == 6) || (x == 12 && y == 6)) {
			p.setLocation("Hall");
		}
		if (x == 18 && y == 5) {
			p.setLocation("Lounge");
		}
		if ((x == 6 && y == 8) || (x == 3 && y == 10)) {
			p.setLocation("Library");
		}
		if ((x == 1 && y == 12) || (x == 5 && y == 15)) {
			p.setLocation("Billiard Room");
		}
		if (x == 4 && y == 19) {
			p.setLocation("Conservatory");
		}
		if ((x == 8 && y == 19) || (x == 15 && y == 19) || (x == 9 && y == 17) || (x == 14 && y == 17)) {
			p.setLocation("Ballroom");
		}
		if (x == 19 && y == 18) {
			p.setLocation("Kitchen");
		}
		if ((x == 16 && y == 12) || (x == 17 && y == 9)) {
			p.setLocation("Dining Room");
		}
	}

	/**
	 * Updates the arraylist holding a Player object's card objects to add a new
	 * card.
	 * 
	 * @param p
	 *            Player whose cards are being updates
	 * @param c
	 *            Card that is being given to the player.
	 */
	public void giveCard(Player p, Card c) {
		ArrayList<Card> a;
		if (p.getCards() == null) {
			a = new ArrayList<Card>();
		} else {
			a = p.getCards();
		}
		a.add(c);
		p.setCards(a);
	}

	/**
	 * Method run when a player makes a guess. Takes in one card of each type,
	 * and then checks to see if anyone has any of those cards. Also moves the
	 * player that is being accused to the same room as the player who is guessing.
	 * 
	 * @param p
	 *            Player making the guess
	 * @param w
	 *            Weapon card in the guess
	 * @param c
	 *            Character card in the guess
	 * 
	 * @return String that tells the player who is denying the guess and the
	 *         card with which they are able to do so
	 */
	public String makeGuess(Player p, Card w, Card c) {
		for(int u = 0; u<6; u++){
			if(_players[u].getName().equals(c.getName())){
				_players[u].setLocation(p.getLocation());
				u = 6;
			}
		}
		int guesses = 0;
		int i = _p;
		while (guesses < 6) {
			i = i + 1;
			if (i == 6) {
				i = 0;
			}

			ArrayList<Card> a = _players[i].getCards();
			for (int j = 0; j < a.size(); j++) {

				if (a.get(j).getName().equals(p.getLocation())) {
					return ("Player: " + _players[i].getName() + " Card: " + p.getLocation());
				}
				if (a.get(j).getName().equals(w.getName())) {
					return ("Player: " + _players[i].getName() + " Card: " + w.getName());
				}
				if (a.get(j).getName().equals(c.getName())) {
					return ("Player: " + _players[i].getName() + " Card: " + c.getName());
				}

			}
			guesses = guesses + 1;
		}
		return "Card not found.";
	}

	/**
	 * Returns the private int holding the index of the current player
	 * 
	 * @return an int representing the player turn (0, 1, 2, or 3)
	 */
	public int returnCurrentPlayer() {
		return _p;
	}

	/**
	 * Sets the player whose turn it currently is. Used by the GUI and the turn
	 * method to change the turn
	 * 
	 * @param i
	 *            the int of the player in _players whose turn it is
	 */
	public void setCurrentPlayer(int i) {
		_p = i;
	}

	/**
	 * Returns the array of players in the game
	 * 
	 * @return Array of players
	 */
	public Player[] returnPlayerArray() {
		return _players;
	}

	/**
	 * Adds a player to the next empty spot in _players. Will not add a player
	 * if _players already has 4 players
	 * 
	 * @param p
	 *            Player who will be added
	 */
	public void addPlayer() {
		_players[0] = new Player("Miss Scarlet");
		_players[1] = new Player("Professor Plum");
		_players[2] = new Player("Mr Green");
		_players[3] = new Player("Mrs White");
		_players[4] = new Player("Mrs Peacock");
		_players[5] = new Player("Colonel Mustard");
	}

	/**
	 * If the player is not in the hallway, it moves them into a valid space
	 * outside of their room location
	 * 
	 * @param p
	 *            Player who will be moving
	 * @param x
	 *            X Coordinate of the new tile
	 * @param y
	 *            Y Coordinate of the new tile
	 * 
	 * @return Whether or not the player successfully exited a room
	 */
	public boolean exitRoom(Player p, int x, int y) {
		if (_board[p.getPlayerYLocation()][p.getPlayerXLocation()] != _corridor) {
			p.setLocation("Hallway");
			p.setPlayerXLocation(x);
			p.setPlayerYLocation(y);
			return true;
		}
		return false;
	}

	/**
	 * If the player is a valid room, it moves them to the corresponding secret
	 * passage
	 * 
	 * @param p
	 *            Player who is moving
	 * 
	 * @return Whether or not a secret passage was successfully taken
	 */
	public boolean secretPassage(Player p) {
		if (p.getLocation() == "Study") {
			p.setLocation("Kitchen");
			return true;
		} else {
			if (p.getLocation() == "Lounge") {
				p.setLocation("Conservatory");
				return true;
			} else {
				if (p.getLocation() == "Kitchen") {
					p.setLocation("Study");
					return true;
				} else {
					if (p.getLocation() == "Conservatory") {
						p.setLocation("Lounge");
						return true;
					} else {
						return false;
					}
				}
			}
		}
	}
	/**Randomly picks a number between 1 and 6
	 * @return int between 1 and 6 */
	public int rollDice(){
		Random r = new Random();
		int min = 1;
		int max = 6;
		return r.nextInt(max - min + 1)+min;
	}
	
	/**Returns the array of rooms used as the board
	 * @return board array */
	public Room[][] getBoard(){
		return _board;
	}
	
	/**Accessor method to return _room
	 * @return _room */
	public Room getRoom(){
		return _room;
	}
	
	public void setGUI(GUI g){
		_g = g;
	}
	
	public Player[] getPlayers(){
		return _players;
	}
}
