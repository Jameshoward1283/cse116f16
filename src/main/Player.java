package main;

import java.util.ArrayList;

/**Class which holds a players location, name, coordinates, and cards/
 * 
 * @author James
 *
 */
public class Player {
	
	/**Player's x coordinate */
	private int x;
	
	/**Player's y coordinate */
	private int y;
	
	/**Name of the room the player is in */
	private String rmName;
	
	/**Array holding the cards that the player has. */
	private ArrayList<Card> myCards;
	
	/**Name of the character the player represents */
	private String characterName;

	/** Creates a Player object
	 * 
	 *  @param s Name of the character represented by the player*/
	public Player(String s) {
		x = 0;
		y = 0;
		//When a player is later created in the Model/GUI class, a loop determining
		//	what character the player is will run and assign the player to their starting tile
		myCards = null;
		//myCards will be filled upon start up of the game.
		characterName = s;
		rmName = "Hallway";
	}

	/**Accessor method for Model
	 * 
	 *  @return X Coordinate of Player*/
	public int getPlayerXLocation() {
		return x;
	}
	
	/**Setter method for Model
	 * 
	 *  @param i new x coordinate of player*/
	public void setPlayerXLocation(int i){
		x = i;
	}

	/**Accessor method for model
	 * 
	 *  @return Y coordinate of Player*/
	public int getPlayerYLocation() {
		return y;
	}
	
	/**Setter method for Model
	 * 
	 *  @param i new Y coordinate for player*/
	public void setPlayerYLocation(int i){
		y = i;
	}

	/**Accessor method for the cards a player holds
	 * 
	 *  @return ArrayList<String> of cards a player holds*/
	public ArrayList<Card> getCards() {
		return myCards;
	}

	/**Accessor method for the character a player represents
	 * 
	 *  @return Name of the player's character*/
	public String getName() {
		return characterName;
	}

	/**Setter method for the player's cards
	 * 
	 *  @param startingCards ArrayList<Card> of the cards the player now holds*/
	public void setCards(ArrayList<Card> startingCards) {
		myCards = startingCards;
	}
	
	/**Setter method for a player's location
	 * 
	 *  @param s New room name of the player*/
	public void setLocation(String s){
		rmName = s;
	}
	
	/**Accessor method for the player's room name
	 * 
	 *  @return Name of the room the player is in*/
	public String getLocation(){
		return rmName;
	}
}
