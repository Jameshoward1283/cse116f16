package Actions;

import main.Player;

/**Event class which changes a player's location and coordinates as they exit a room
 * 
 * @author Charles
 *
 */
public class ExitRoom {
	
	/**Player who is affected by this class */
	private Player _p;
	
	/**Sets the player's location to hallway and updates their coordinates to reflect the move
	 * In the gui, it will also decrease the roll count by 1
	 * 
	 *  @param x new X coordinate of the player
	 *  @param y new Y coordinate of the player*/
	public ExitRoom(int x, int y){
		_p.setLocation("Hallway");
		_p.setPlayerXLocation(x);
		_p.setPlayerXLocation(y);
	}

}
