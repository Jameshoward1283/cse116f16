package Actions;

import main.Model;

/**Event class which returns a direction to model for use in the movePlayer method
 * 
 * @author Charles
 *
 */
public class moveDirection {
	
	/**Model which is being modified */
	private Model _m;
	
	/**Sets _direct in Model to the string s, which is given by the GUI */
	public moveDirection(String s){
		//s will be determined by a button in the gui
		_m.setDirect(s);
	}

}
