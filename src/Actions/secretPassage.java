package Actions;

import main.Player;

/**Event class which allows a player to use one of the secret passages
 * 
 * @author Charles
 *
 */
public class secretPassage {

	/**Player who this method is being called on */
	private Player _p;
	
	/**Changes the player's rmName depending on what room they are currently in */
	public secretPassage(Player p){
		if(p.getLocation() == "Study"){
			p.setLocation("Kitchen");
		}
		if(p.getLocation() == "Lounge"){
			p.setLocation("Conservatory");
		}
		if(p.getLocation() == "Kitchen"){
			p.setLocation("Study");
		}
		if(p.getLocation() == "Conservatory"){
			p.setLocation("Lounge");
		}
	}
}
