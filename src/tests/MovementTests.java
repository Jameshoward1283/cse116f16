package tests;
import static org.junit.Assert.*;

import org.junit.Test;

import main.*;
	/**
	 * Tests to verify that movements are working correctly, and that illegal movements are
 	 * properly disallowed
 	 * 
 	 * @author Evan Camizzi
 	 * @author Charles Henneberger
 	 */

public class MovementTests {
	
	/**Model and player required to run the tests */
	private Model _m;
	private Player _p;
	
	/**Instantiates the model and the player */
	public MovementTests(){
		_m = new Model();
		_p = new Player("Mr Test");
	}
	
	/** Personal Note: My head hurts.
	 * 	This test loop is to ascertain nearly every circumstance for a test and ultimately
	 *  make sure our entire movement system is working.
	 */
	
//	@Test
//	public void CanWeMoveHorizontallyTest()
//	{	Model m = new Model();
//		m.fillBoard();
//		Player p = new Player("Scarlet");
//		int iX = 17;
//		int iY = 16;
//		p.setPlayerXLocation(iX);p.setPlayerYLocation(iY); //These two need to be coded into startPoints
//		int fX = 5;
//		int fY = 0;
//		int expectedXLoc = iX + fX;
//		int expectedYLoc = iX + fY;
//		int movement = 5;
//		while(movement > 0){m.setDirect("right");m.movePlayer(p);movement = movement -1;System.out.print(p.getPlayerXLocation());System.out.print(", ");}
//		assertTrue(expectedXLoc == p.getPlayerXLocation() && expectedYLoc == p.getPlayerYLocation());
//	}
//	
//	@Test
//	public void CanWeMoveVerticallyTest()
//	{	Model m = new Model();
//		m.fillBoard();
//		Player p = new Player("Scarlet");
//		int iX = 23;
//		int iY = 16;
//		p.setPlayerXLocation(iX);p.setPlayerYLocation(iY); //These two need to be coded into startPoints
//		int fX = 0;
//		int fY = -5;
//		int expectedXLoc = iX + fX;
//		int expectedYLoc = iY + fY;
//		int movement = 5;
//		while(movement > 0){m.setDirect("down");m.movePlayer(p);movement = movement -1;System.out.print(p.getPlayerYLocation());System.out.print(", ");}
//		assertTrue(expectedXLoc == p.getPlayerXLocation() && expectedYLoc == p.getPlayerYLocation());
//	}
	
	/*
	 * This was a pain. My head hurts trying to fix it. Will do so later.
	 */
//	public boolean movementTest(int iX, int iY, int fX, int fY, int movement) //Copy and Paste whole method (until loop), make changes to fX, fY to move.
//	{	if(movement > 6){return false;}
//		Model m = new Model();
//		m.fillBoard();
//		Player p = new Player("Scarlet");
//		p.setPlayerXLocation(iX);p.setPlayerYLocation(iY);
//		//if(p.getLocation() != "Hallway" | p.getLocation() != "Room" | p.getLocation() != "Door"){System.out.print(p.getLocation());System.out.print("is an incorrect starting point.");return false;}
//		int changeInHorizontal = Math.abs(Math.subtractExact(iX , fX));
//		int changeInVertical = Math.abs(Math.subtractExact(iY , fY));
//		if(changeInHorizontal + changeInVertical > 6){System.out.print("You wont be able to reach there.");return false;}
//		int currentPlayerXLocation = p.getPlayerXLocation();
//		int currentPlayerYLocation = p.getPlayerYLocation();
//		while(movement > 0)
//		{   while(currentPlayerXLocation != fX)
//			{
//				if(fX > currentPlayerXLocation)
//				{	m.setDirect("right");
//					m.movePlayer((/*Math.subtractExact(fX, currentPlayerXLocation)*/movement), p);
//					currentPlayerXLocation = currentPlayerXLocation + 1;
//					movement = movement - 1;
//					System.out.print("Moved Right, ");
//					System.out.print("Player X location currently is: ");System.out.print(p.getPlayerXLocation());
//					System.out.print(" .");
//				}
//				else if(fX < currentPlayerXLocation)
//				{	m.setDirect("left");
//					m.movePlayer((/*Math.subtractExact(currentPlayerXLocation, fX)*/movement), p);
//					currentPlayerXLocation = currentPlayerXLocation - 1;
//					movement = movement - 1;
//					System.out.print("Moved Left, ");
//				}
//				else if(fX == currentPlayerXLocation){
//					while(currentPlayerYLocation != fY)
//					{	if(fY > currentPlayerYLocation)
//						{	m.setDirect("up");
//							m.movePlayer((Math.subtractExact(fY, currentPlayerYLocation)), p);
//							currentPlayerYLocation = currentPlayerYLocation + 1;
//							movement = movement - 1;
//							System.out.print("Moved Up, ");
//						}
//						else if(fY < currentPlayerYLocation)
//						{	m.setDirect("down");
//							m.movePlayer((Math.subtractExact(currentPlayerYLocation, fY)), p);
//							currentPlayerYLocation = currentPlayerYLocation - 1;
//							movement = movement - 1;
//							System.out.print("Moved Down, ");
//						}
//						else if(currentPlayerXLocation == fX && currentPlayerYLocation == fY){return true;}	
//					}
//				}
//			}	
//			
//		}
//		return false;
//	}
//	
//	@Test public void moveRightSix(){assertTrue(movementTest(0, 16, 6, 16, 6));}
//	@Test public void movementTestSample02(){assertFalse(movementTest(0, 0, 0, 6, 7));}
//	@Test public void movementTestSample03(){assertTrue(movementTest(0, 0, 0, 0, 0));}
//	@Test public void movementTestSample04(){assertTrue(movementTest(0, 0, 0, 0, 0));}
//	@Test public void movementTestSample05(){assertTrue(movementTest(0, 0, 0, 0, 0));}
//	@Test public void movementTestSample06(){assertTrue(movementTest(0, 0, 0, 0, 0));}
	
	
	//As a general note, our move method does not do aggregate moving,
		//	but rather moves 1 tile at a time.
		
		/**Test that your project correctly identifies as legal a move that only goes horizontally for as many squares as the die roll */
		@Test
		public void horizontal(){
			int roll = 6;
			_p.setPlayerXLocation(15);
			_p.setPlayerYLocation(7);
			while(roll > 0){
				boolean b = _m.movePlayer("left", _p);
				assertTrue(b);
				roll -= 1;
			}
			_p.setPlayerXLocation(15);
			_p.setPlayerYLocation(7);
			roll = 7;
			while(roll>0){
				boolean b = _m.movePlayer("right", _p);
				assertTrue(b);
				roll -= 1;
			}
		}
		
		/**Test that your project correctly identifies as legal a move that only goes vertically for as many squares as the die roll */
		@Test
		public void vertical(){
			int roll = 4;
			_p.setPlayerXLocation(7);
			_p.setPlayerYLocation(9);
			while(roll > 0){
				boolean b = _m.movePlayer("up", _p);
				assertTrue(b);
				roll -= 1;
			}
			_p.setPlayerXLocation(7);
			_p.setPlayerYLocation(9);
			roll = 10;
			while(roll>0){
				boolean b = _m.movePlayer("down", _p);
				assertTrue(b);
				roll -= 1;
			}
		}
		
		/**Test that your project correctly identifies as legal a move that goes horizontally & vertically for as many squares as the die roll */
		@Test
		public void combo(){
			int roll = 8;
			_p.setPlayerXLocation(5);
			_p.setPlayerYLocation(19);
			while(roll>0){
				boolean b = _m.movePlayer("up", _p);
				assertTrue(b);
				roll -= 1;
				b = _m.movePlayer("right", _p);
				assertTrue(b);
				roll -= 1;
			}
		}
		
		/**Test that your project correctly identifies as legal a move that goes through a door and into a room */
		@Test
		public void enterRoom(){
			_p.setPlayerXLocation(6);
			_p.setPlayerYLocation(6);
			int roll = 4;
			while(roll > 0){
				boolean b = _m.movePlayer("up", _p);
				assertTrue(b);
				roll -= 1;
				if(!_p.getLocation().equals("Hallway")){
					roll *= 0;
				}
			}
			assertTrue(_p.getLocation().equals("Study"));
		}
		
		/**Test that your project correctly identifies as legal a move that just uses the a secret passageway */
		@Test
		public void secretTunnel(){
			_p.setLocation("Study");
			boolean b = _m.secretPassage(_p);
			assertTrue(b);
			assertTrue(_p.getLocation().equals("Kitchen"));
			b = _m.secretPassage(_p);
			assertTrue(b);
			assertTrue(_p.getLocation().equals("Study"));
			_p.setLocation("Lounge");
			b = _m.secretPassage(_p);
			assertTrue(b);
			assertTrue(_p.getLocation().equals("Conservatory"));
			b = _m.secretPassage(_p);
			assertTrue(b);
			assertTrue(_p.getLocation().equals("Lounge"));
			_p.setLocation("Hall");
			b = _m.secretPassage(_p);
			assertFalse(b);
			assertTrue(_p.getLocation().equals("Hall"));
		}
		
		/**Test that your project correctly identifies as illegal a move that goes more squares than the die roll */
		@Test
		public void plusRoll(){
			//it is not possible to move more than the roll 
			//	with our move method
			assertFalse(false);
		}
		
		/**Test that your project correctly identifies as illegal a move that goes diagonally */
		@Test
		public void diagonal(){
			//it is not possible to move diagonally with our method
			assertFalse(false);
		}
		
		/**Test that your project correctly identifies as illegal a move that is not contiguous */
		@Test
		public void skips(){
			//it is not possible to move to a non-contiguous space with
			//	our method
			assertFalse(false);
		}
		
		/**Test that your project correctly identifies as illegal a move that goes through a wall */
		@Test
		public void wallWalk(){
			_p.setPlayerXLocation(1);
			_p.setPlayerYLocation(4);
			boolean b = _m.movePlayer("up", _p);
			assertFalse(b);
			assertTrue(_p.getPlayerYLocation() == 4);
			_p.setPlayerXLocation(7);
			_p.setPlayerYLocation(0);
			b = _m.movePlayer("right", _p);
			assertFalse(b);
			assertTrue(_p.getPlayerXLocation() == 7);
		}
}


