package tests;

import static org.junit.Assert.*;

import org.junit.Test;

import main.Model;
import main.Player;
import main.Room;
import main.Card;

/**Suite of tests checking the use of the makeGuess method.
 * 
 * NOTE: This test case will no longer pass any tests, since the game
 * was updated to run with 6 players instead of 4
 * 
 * @author James
 *
 */
public class SuggestionTests {
	
	/**Instance of model and its methods */
	private Model _m;
	
	private Player[] _ps = _m.getPlayers();
	
	/**6 players */
	private Player _p = _ps[0];
	private Player _p1 = _ps[1];
	private Player _p2 = _ps[2];
	private Player _p3 = _ps[3];
	private Player _p4 = _ps[4];
	private Player _p5 = _ps[5];
	
	/**4 weapon cards the players will be holding */
	private Card _knife;
	private Card _pipe;
	private Card _rope;
	private Card _wrench;
	
	/**4 room cards the players will be holding */
	private Card _hall;
	private Card _lounge;
	private Card _kitchen;
	private Card _study;
	
	/**4 suspect cards the players will be holding */
	private Card _grand;
	private Card _theft;
	private Card _auto;
	private Card _five;
	
	/**Instantiates the game, adds the players to model, and gives each of them 3 cards */
	public SuggestionTests(){
		_m = new Model();
		
		_knife = new Card("Knife", "Weapon");
		_m.giveCard(_p, _knife);
		_hall = new Card("Hall", "Room");
		_m.giveCard(_p, _hall);
		_grand = new Card("Grand", "Suspect");
		_m.giveCard(_p, _grand);
		
		_pipe = new Card("Lead Pipe", "Weapon");
		_m.giveCard(_p1, _pipe);
		_lounge = new Card("Lounge", "Room");
		_m.giveCard(_p1, _lounge);
		_theft = new Card("Theft", "Suspect");
		_m.giveCard(_p1, _theft);
		
		_rope = new Card("Rope", "Weapon");
		_m.giveCard(_p2, _rope);
		_kitchen = new Card("Kitchen", "Room");
		_m.giveCard(_p2, _kitchen);
		_auto = new Card("Auto", "Suspect");
		_m.giveCard(_p2, _auto);
		
		_wrench = new Card("Wrench", "Weapon");
		_m.giveCard(_p3, _wrench);
		_study = new Card("Study", "Room");
		_m.giveCard(_p3, _study);
		_five = new Card("Five", "Suspect");
		_m.giveCard(_p3, _five);
	}
	
	/**Suggestion would be answered by the next player because they have the Player card */
	@Test
	public void firstPlayer(){
		_m.setCurrentPlayer(0);
		_p.setLocation("Library");
		String g1 = _m.makeGuess(_p, _knife, _theft);
		assertTrue(g1.equals("Player: Red Card: Theft"));
	}
	
	/**Suggestion would be answered by the next player because they have the Room card */
	@Test
	public void firstRoom(){
		_m.setCurrentPlayer(2);
		_p2.setLocation("Study");
		String s = _m.makeGuess(_p2, _rope, _auto);
		assertTrue(s.equals("Player: Redemption Card: Study"));
	}
	
	/**Suggestion would be answered by the next player because they have the Weapon card */
	@Test
	public void firstWeapon(){
		_m.setCurrentPlayer(3);
		_p3.setLocation("Conservatory");
		String s = _m.makeGuess(_p3, _knife, _five);
		assertTrue(s.equals("Player: Mr Test Card: Knife"));
	}
	
	/**Suggestion would be answered by the next player because they have 2 matching cards */
	@Test
	public void firstDouble(){
		_m.setCurrentPlayer(1);
		_p1.setLocation("Kitchen");
		String s = _m.makeGuess(_p1, _wrench, _grand);
		assertTrue(s.equals("Player: Dead Card: Kitchen"));
	}
	
	/** Suggestion would be answered by the player after the next player because they have 1 or more matching cards */
	@Test
	public void secondMulti(){
		_m.setCurrentPlayer(0);
		_p.setLocation("Lounge");
		String s = _m.makeGuess(_p, _pipe, _theft);
		assertTrue(s.equals("Player: Red Card: Lead Pipe"));
	}
	
	/**Suggestion would be answered by the player immediately before player making suggestion because they have 1 or more matching cards */
	@Test
	public void thirdMulti(){
		Card revolver = new Card("Revolver", "Weapon");
		_m.setCurrentPlayer(0);
		_p.setLocation("Study");
		String s = _m.makeGuess(_p, revolver, _five);
		assertTrue(s.equals("Player: Redemption Card: Study"));
	}
	
	/**Suggestion cannot be answered by any player but the player making the suggestion has 1 or more matching cards */
	@Test
	public void IHaveMe(){
		Card candle = new Card("Candlestick", "Weapon");
		_m.setCurrentPlayer(2);
		_p2.setLocation("Conservatory");
		String s = _m.makeGuess(_p2, candle, _auto);
		assertTrue(s.equals("Player: Dead Card: Auto"));
	}
	
	/**Suggestion cannot be answered by any player and the player making the suggestion does not have any matching cards */
	@Test
	public void noHeldCard(){
		Card candle = new Card("Candlestick", "Weapon");
		Card rockstar = new Card("Rockstar", "Suspect");
		_m.setCurrentPlayer(3);
		_p3.setLocation("Billiards Room");
		String s = _m.makeGuess(_p3, candle, rockstar);
		assertTrue(s.equals("Card not found."));
	}

}
