In the Model class, there is a method called addPlayer() that adds
	all 6 players (in order) to the array of players _players.
	This can be seen in the GUI on the board by the six colored
	tiles representing each player.
	
The model also defines an integer _p that defines the player whose
	turn it currently is and begins at 0. At the end of each players trun, it
	increases by 1, indicating a new player. When it hits 6, it
	resets back to 0. The player whose turn it currently is is 
	displayed at the top of the GUI, and rotates in order, which can
	be seen by playing through a full round of turns.